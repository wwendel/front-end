# Welcome to Net-Inspect

A major functionality of our application is to generate lists of items in our system. In this exercise, you will build a table that lists "Qualified Parts", or "QPL".

### Background
Net-Inspect is developing a new web application. This web application uses an existing MS SQL database with some initial data. You are leading the project and have been tasked with your first statement of work. Ron, one of the product managers at Net-Inspect, has just emailed you the details of the statement of work. In the email, you were given a PDF document describing the scope of the statement of work including the mock-ups and business rules.

### Action 
Please review the PDF document �qpl mockups - part 1.pdf� before reading further.

## Specification
Consuming a provided API, you are to build a table that will allow users to browse Qualified Parts in the system. This report should:

1. Contain all the columns listed in the mockup
2. Provide pagination of the qualified parts through the use of our provided REST API
3. Allow users to perform simple searches

## Task Info
You may use the environment provided, or set up one of your own. Please feel free to use any libraries as you see fit, but please avoid using a grid or table library to circumvent the bulk of the task. Your grid should look similar to the mockup. It doesn't need to be pixel-perfect, but it should be close.

### Getting Started

You are free to use your own environment if you like, but we have also provided a setup for you. If you'd like to use our development environment, please follow these steps:

1.  Clone the repository locally 
2.  Ensure you have node, npm, gulp, typescript, sass, and node-sass installed (```npm i gulp typescript node-sass```)
3.  Run ```npm install``` 
4.  Run ```gulp``` to watch files for changes 
5.  Set up a server of your choice to serve files 
6.  Commit and push your changes as you go

If you have any trouble with these steps, please get in touch, or feel free to use your own tooling.

We have provided an API for you to consume in order to get this data here:
```
GET https://exam.net-inspect.com/qpl?offset={offset}&pageSize={pageSize}
```

You may assume a constant total of 100 items available in the database. When making API calls, be sure to include the proper authorization headers to identify yourself to server:
```
Authorization: Key {YOUR_AUTHORIZATION_HEADER}
```

We would like to see:

*  Proficiency with a modern JavaScript framework 
*  Proficiency with TypeScript (preferred) or ES6 
*  Ability to thoughtfully write reusable, maintainable, and clean code 
*  Proficiency with SCSS 
*  Adherence to modern UX best practices 
*  Basic regard for performance

If you have any questions, please get in touch.