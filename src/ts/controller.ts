namespace QualifiedPartsList {
    
    export class QualifiedPartsListController {
        
        static $inject = ["$scope"];

        constructor(
            private readonly $scope
        ) {
            
            $scope.title = "Qualified Parts List";

        }

    }

}